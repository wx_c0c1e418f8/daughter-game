using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using QFramework;
using TMPro;
using UnityEngine.UI;

//这个脚本被挂载到了Panel上
public class Controller : MonoBehaviour, IController
{
    public Text Text;
    public Model model;
    public IArchitecture GetArchitecture()
    {
        return GameArchitecture.Interface;
    }

    void Start()
    {
        model = this.GetModel<Model>();
        //监听数据改变事件
        this.RegisterEvent<CountChangeEvent>((e) =>
        {
            Text.text = "number : " + model.number;
        });
    }



    //给按钮提供的
    public void add()
    {
        this.GetSystem<syStem>().Add();
    }
    public void subtract()
    {
        this.GetSystem<syStem>().Subtract();
    }
}
