using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using QFramework;
public class GameArchitecture : Architecture<GameArchitecture>
{
    protected override void Init()
    {
        //Model
        this.RegisterModel<Model>(new Model());
        //System
        this.RegisterSystem<syStem>(new syStem());
    }
}
