using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using QFramework;


// 数据变更事件
public struct CountChangeEvent // ++
{

}



public class syStem : AbstractSystem
{
    public Model model;
    protected override void OnInit()
    {
        model = this.GetModel<Model>();
    }

    public void Add()
    {
        model.number++;
        this.SendEvent<CountChangeEvent>();
    }
    public void Subtract()
    {
        model.number--;
        this.SendEvent<CountChangeEvent>();
    }
}
