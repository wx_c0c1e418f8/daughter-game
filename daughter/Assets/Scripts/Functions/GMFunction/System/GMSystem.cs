﻿using QFramework;

namespace Functions.GM
{
    //整个模块的逻辑实现和分发
    public class GMSystem : ISystem
    {
        private IArchitecture gmArchitecture;
        public void Init()
        {

        }

        public IArchitecture GetArchitecture()
        {
            return gmArchitecture;
        }

        public void SetArchitecture(IArchitecture architecture)
        {
            this.gmArchitecture = architecture;
        }

    }
}
