﻿using QFramework;
using UnityEditor.SceneManagement;

namespace Functions.GM
{

    // 定义一个架构（提供 MVC、分层、模块管理等）
    // 可添加对外接口
    public class GMArchitecture : Architecture<GMArchitecture>
    {
        protected override TResult DoQuery<TResult>(IQuery<TResult> query)
        {
            //// 注册 System 
            //this.RegisterSystem(new AchievementSystem()); // +

            //// 注册 Model
            //this.RegisterModel(new CounterAppModel());

            //// 注册存储工具的对象
            //this.RegisterUtility(new Storage());

            return base.DoQuery(query);
        }

        protected override TResult ExecuteCommand<TResult>(ICommand<TResult> command)
        {
            return base.ExecuteCommand(command);
        }

        protected override void ExecuteCommand(ICommand command)
        {
            base.ExecuteCommand(command);
        }

        protected override void Init()
        {
            
        }
    }
}
