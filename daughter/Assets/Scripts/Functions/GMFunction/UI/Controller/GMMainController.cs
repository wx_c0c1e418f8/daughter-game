using Functions.GM;
using QFramework;
using UnityEngine;
using UnityEngine.UI;


// Controller [页面逻辑的实现和分发]
public class GMMainController : MonoBehaviour, IController
{
    public Text text;
    public Button Btn;
    //...
    // Model
    private GMMainModel mModel;

    public IArchitecture GetArchitecture()
    {
        return GMArchitecture.Interface;
    }

    //...
}
